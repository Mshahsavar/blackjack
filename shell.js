console.log(mdc);

const MDCTopAppBar = mdc.topAppBar.MDCTopAppBar;
const topAppBarElement = document.querySelector('.mdc-top-app-bar');
new MDCTopAppBar(topAppBarElement);

const MDCTextField = mdc.textField.MDCTextField;
let textFields = [].map.call(document.querySelectorAll('.mdc-text-field'), function (el) {
    return new MDCTextField(el);
});

const suits = ["♠", "♡", "♢", "♣"];
const ranks = ["Ace", 2, 3, 4, 5, 6, 7, 8, 9, 10, "Knave", "Queen", "King"];
// Global variables to track the deck ID and remaining cards
let deckId = null;
let remainingCards = 312;  // 6 decks x 52 cards each

//function getDeck() {
//    deck = [];
//    for (let suit of suits) {
//        for (let rank of ranks) {
//            deck.push({ suit, rank });
//        }
//    }
//    return deck;
//}
async function fetchNewDeck() {
    const response = await fetch('https://www.deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1');
    const data = await response.json();
    deckId = data.deck_id;
}

// Fetch a new shoe of 6 decks
function getShoe(callback) {
    fetch('https://www.deckofcardsapi.com/api/deck/new/shuffle?deck_count=6')
        .then(response => response.json())
        .then(data => {
            deckId = data.deck_id;
            remainingCards = data.remaining;
            if (callback) callback(data);
        })
        .catch(error => console.error('Error fetching new shoe:', error));
}

// Draw four cards from the deck
function drawFourCards(callback) {
    if (!deckId) {
        console.error('No deck initialized');
        return;
    }

    fetch(`https://www.deckofcardsapi.com/api/deck/${deckId}/draw?count=4`)
        .then(response => response.json())
        .then(data => {
            remainingCards = data.remaining;  // Update the remaining cards count
            if (callback) callback(data.cards);
        })
        .catch(error => console.error('Error drawing cards:', error));
}

function getRandomCard() {
    if (!deck.length) {
        getDeck();
    }
    let randomIndex = Math.floor(Math.random() * deck.length);
    return deck[randomIndex];
}

const mapRanksToWords = {
    2: "Two", 3: "Three", 4: "Four", 5: "Five", 6: "Six", 7: "Seven", 8: "Eight", 9: "Nine", 10: "Ten", "Knave": "Knave", "Queen": "Queen", "King": "King", "Ace": "Ace"
};

const mapSuitsToWords = {
    "♠": "Spades", "♡": "Hearts", "♢": "Diamonds", "♣": "Clubs", "": "Mystery"
};

function rankToWord(rank) {
    return mapRanksToWords[rank] || "Mystery";
}

function suitToWord(suit) {
    return mapSuitsToWords[suit] || "Mystery";
}

function rankToValue(rank) {
    if (['Knave', 'Queen', 'King'].includes(rank)) {
        return "10";
    } else if (rank === 'Ace') {
        return "11/1";
    } else if (rank === 'Face Down') {
        return "?";
    }else {
        return rank.toString();
    }
    
}

function dealToDisplay(card) {
    const cardElement = document.createElement('li');
    const cardImage = document.createElement('img');
    cardImage.src = hide ? card.image : card.cards[0].image;
    cardImage.alt = hide ? "Face Down" : `${rankToWord(card.value)} of ${suitToWord(card.suit)}`;
    cardElement.appendChild(cardImage);
    document.querySelector(targetSelector).appendChild(cardElement);
}

function dealRandomCard() {
    dealToDisplay(getRandomCard());
}


let bankroll = 2022;  // Initialize the player's bankroll to 2022

function getBankroll() {
    return bankroll;
}

function setBankroll(newBalance) {
    bankroll = newBalance;
}

function timeToBet() {
    // Hide player's actions
    document.getElementById('playersActions').style.display = 'none';

    // Show the betting section
    const bettingSection = document.getElementById('betting');
    bettingSection.style.display = 'block'; // Make sure the betting section is visible

    // Ensure the input field is visible and has a minimum width set
    const wagerInput = document.getElementById('users-wager');
    if (wagerInput) {
        wagerInput.style.display = 'inline-block'; // Make sure the input is displayed
        wagerInput.style.minWidth = '100px'; // Set a minimum width to ensure visibility
        wagerInput.focus(); // Optionally, set focus to the input field
    }

    // Update bankroll display
    document.getElementById('bankrollDisplay').textContent = `$${getBankroll()}`;
}



function makeWager() {
    const wagerAmount = document.getElementById('users-wager').value;
    console.log(wagerAmount);
    timeToPlay();
}

function timeToPlay() {
    document.getElementById('playersActions').style.display = 'block';
    document.getElementById('betting').style.display = 'none';
}

// Get the bankroll from localStorage or default to 2022 if not present
function getBankroll() {
    const bankroll = localStorage.getItem('bankroll');
    return bankroll ? parseInt(bankroll, 10) : 2022;
}

// Set the bankroll in localStorage
function setBankroll(newBalance) {
    localStorage.setItem('bankroll', newBalance.toString());
}


document.addEventListener('DOMContentLoaded', () => {
    getShoe(); 
    // Add event listener for the "Bet" button
    const betButton = document.getElementById('bet-button');
    if (betButton) {
        betButton.addEventListener('click', makeWager);
    }
    
    const hitButton = document.getElementById('hit-button');
    if (hitButton) {
        hitButton.addEventListener('click', dealRandomCard);
    }

    const form = document.getElementById('eligibilityForm');
    if (form) {
        form.addEventListener('submit', (event) => {
            event.preventDefault();
            let isEligible = true;

            // Explicitly log expected strings
            const fullNameInput = document.querySelector('input[name="fullName"]');
            console.log(`Full Name: ${fullNameInput.value}`);

            const usernameInput = document.querySelector('input[name="username"]');
            console.log(`Username: ${usernameInput.value}`);

            const passwordInput = document.querySelector('input[name="password"]');
            console.log(`Enter Password: ${passwordInput.value}`);

            const confirmPasswordInput = document.querySelector('input[name="confirmPassword"]');
            console.log(`Confirm Password: ${confirmPasswordInput.value}`);

            const ageInput = document.querySelector('input[name="age"]');
            console.log(`Age: ${ageInput.value}`);

            const birthDateInput = document.querySelector('input[name="birthDate"]');
            console.log(`Birth Date: ${birthDateInput.value}`);

            if (!fullNameInput.value.trim()) {
                console.log("Full Name cannot be empty. The user is ineligible.");
                isEligible = false;
            }

            if (!usernameInput.value.trim()) {
                console.log("Username cannot be empty. The user is ineligible.");
                isEligible = false;
            }
            // Checkbox logging
            const legalCheckbox = document.getElementById('legal-checkbox');
            const termsCheckbox = document.getElementById('terms-checkbox');
            console.log(`The user has ${legalCheckbox.checked ? '' : 'not '}checked the legal checkbox`);
            console.log(`The user has ${termsCheckbox.checked ? '' : 'not '}checked the terms checkbox`);

            // Eligibility checks
            if (!legalCheckbox.checked || !termsCheckbox.checked) {
                isEligible = false;
            }

            if (passwordInput.value !== confirmPasswordInput.value) {
                isEligible = false;
            }

            const age = parseInt(ageInput.value, 10);
            if (isNaN(age) || age < 13) {
                isEligible = false;
            }

            // Log the final eligibility
            console.log(`The user is ${isEligible ? 'eligible' : 'ineligible'}`);
        });
    }
});